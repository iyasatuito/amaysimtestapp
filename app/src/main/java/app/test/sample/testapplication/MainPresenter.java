package app.test.sample.testapplication;

import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by pia on 21/12/16.
 */
public class MainPresenter {

    private MainActivity mMainView;

    public MainPresenter(String user) {
        try {
            JSONObject userJson = new JSONObject(user);
            User mUser = new User(userJson);
            setUserInfo(mUser);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onCurrentView(MainActivity view) {
        this.mMainView = view;
    }

    private void setUserInfo(final User user) {
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                mMainView.setUsername(user.getTitle(), user.getFirstName(), user.getLastName());
                mMainView.setProductName(user.getProductName());
                mMainView.setProductPrice(convertDecimal(user.getProductPrice()));
                mMainView.setSubscription(convertDecimal(user.getSubscriptionDataBalance()));
            }
        }, 50);
    }


    private String convertDecimal(int i){
        int numberOfDecimalPlace = 2;
        BigDecimal unscaled = new BigDecimal(i);
        BigDecimal scaled = unscaled.scaleByPowerOfTen(-numberOfDecimalPlace);
        return "$"+String.valueOf(scaled);
    }

}
