package app.test.sample.testapplication;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by pia on 21/12/16.
 */
public interface WebServices {

    @GET("/engineering-test-resources/master/collection.json")
    void getUserCredentials(
            Callback<User> response);

}
