package app.test.sample.testapplication;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by pia on 21/12/16.
 */
public class ServiceGenerator {

    private static ServiceGenerator instance = new ServiceGenerator();
    private static WebServices apiService;
    private static String BASE_URL = "https://raw.githubusercontent.com/amaysim-au";

    private ServiceGenerator() {

    }

    public synchronized static ServiceGenerator getInstance() {
        if (instance == null)
            instance = new ServiceGenerator();
        return instance;
    }

    public WebServices getApiService() {
        return apiService;
    }

    public void generateRest() {

        RestAdapter.Builder builderAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setClient(new OkClient(getClient()));

        RestAdapter restAdapter = builderAdapter.build();
        apiService = restAdapter.create(WebServices.class);
    }


    private static OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);
        return client;
    }


}
