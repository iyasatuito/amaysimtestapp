package app.test.sample.testapplication;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    @Bind(R.id.etEmail)
    EditText mEmail;
    @Bind(R.id.etPassword)
    EditText mPassword;
    @Bind(R.id.btSignin)
    Button mButton;

    private LoginPresenter mLoginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mButton.setOnClickListener(this);

        if(hasNetworkConnection()){
            mLoginPresenter = new LoginPresenter();
            mLoginPresenter.onTakeView(this);
        } else {
           setError("Connection error");
        }

    }

    private boolean hasNetworkConnection() {
        ConnectivityManager conMgr = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        return i == null?false:(!i.isConnected()?false:i.isAvailable());
    }

    public void setUsernameError(String msg) {
        mEmail.requestFocus();
        mEmail.setError(msg);
    }

    public void setPasswordError(String msg) {
        mPassword.requestFocus();
        mPassword.setError(msg);
    }


    public void navigateSplashScreen(String mBody) {
        Intent intent = new Intent(this, WelcomeActivity.class);
        intent.putExtra("user", mBody);
        startActivity(intent);
        finish();
    }


    @Override
    public void onClick(View v) {
        mLoginPresenter.validateCredentials(mEmail.getText().toString(), mPassword.getText().toString());
    }

    public void setError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
