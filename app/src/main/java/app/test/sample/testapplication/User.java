package app.test.sample.testapplication;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by pia on 20/12/16.
 */
public class User implements Serializable {

    private static final long serialVersionUID = -5231502930567812762L;

    private String mTitle;
    private String mFirstName;
    private String mLastName;
    private int mSubscriptionDataBalance;
    private String mProductName;
    private int mProductPrice;


    public User(JSONObject data)  {
        try {
            JSONObject dataNode = data.getJSONObject("data");
            JSONObject attributesNode = dataNode.getJSONObject("attributes");
            this.mTitle = attributesNode.optString("title");
            this.mFirstName = attributesNode.optString("first-name");
            this.mLastName = attributesNode.optString("last-name");

            JSONArray jArr = data.getJSONArray("included");
            for (int i = 0; i < jArr.length(); i++) {

                JSONObject obj2 = jArr.getJSONObject(1);
                JSONObject att2 = obj2.getJSONObject("attributes");
                mSubscriptionDataBalance = att2.optInt("included-data-balance");

                JSONObject obj3 = jArr.getJSONObject(2);
                JSONObject att3 = obj3.getJSONObject("attributes");
                mProductPrice = att3.optInt("price");
                mProductName = att3.optString("name");

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getTitle() {
        return mTitle;
    }

    public int getSubscriptionDataBalance() {
        return mSubscriptionDataBalance;
    }

    public String getProductName() {
        return mProductName;
    }

    public int getProductPrice() {
        return mProductPrice;
    }
}
