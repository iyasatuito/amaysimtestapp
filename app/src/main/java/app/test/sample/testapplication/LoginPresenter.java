package app.test.sample.testapplication;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by pia on 21/12/16.
 */
public class LoginPresenter {

    private String mUserEmail;
    private String mResponseBody;
    private LoginActivity mView;

    public LoginPresenter() {
        WebServices webServices = ServiceGenerator.getInstance().getApiService();
        webServices.getUserCredentials(
                new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        try {
                            mResponseBody = getResponseBody(response);
                            JSONObject json = new JSONObject(mResponseBody);
                            JSONObject data = json.getJSONObject("data");
                            JSONObject attributes = data.getJSONObject("attributes");
                            mUserEmail = attributes.optString("email-address");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        mView.setError(retrofitError.toString());

                    }
                });

    }

    public void onTakeView(LoginActivity view) {
        this.mView = view;
    }


    public void validateCredentials(String mEmail, String mPassword) {
        if (mEmail.isEmpty() ||
                mEmail.trim().isEmpty()) {
            mView.setUsernameError("Email cannot be empty");
        } else if (!mEmail.equals(mUserEmail)) {
            mView.setUsernameError("Please key in a valid email address");
        } else if (mPassword.isEmpty() ||
                mPassword.isEmpty()) {
            mView.setPasswordError("Password cannot be empty");
        } else if (!mPassword.equals("test")) {
            mView.setPasswordError("Email and password do not match");
        } else {
            mView.navigateSplashScreen(mResponseBody);
        }
    }

    public String getResponseBody(Response response) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
