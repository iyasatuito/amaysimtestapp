package app.test.sample.testapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by pia on 21/12/16.
 */
public class MainActivity extends AppCompatActivity {


    @Bind(R.id.tvUserName)
    TextView mUserName;
    @Bind(R.id.tvSubscription)
    TextView mSubscription;
    @Bind(R.id.tvProduct)
    TextView mProductName;
    @Bind(R.id.tvPrice)
    TextView mProductPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activtiy_information);
        ButterKnife.bind(this);

        MainPresenter mPresenter = new MainPresenter(getIntent().getStringExtra("user"));
        mPresenter.onCurrentView(this);

    }


    public void setUsername(String title, String firstName, String lastName) {
        mUserName.setText(title+ ". "+ firstName + " " +lastName);
    }

    public void setSubscription(String subscription) {
        mSubscription.setText(subscription);
    }

    public void setProductName(String productName) {
        mProductName.setText(productName);
    }

    public void setProductPrice(String subscription) {
        mProductPrice.setText(subscription);
    }

}
