package app.test.sample.testapplication;

import android.app.Application;

/**
 * Created by pia on 21/12/16.
 */
public class AmeysimApplication extends Application {

    private ServiceGenerator serviceGenerator = ServiceGenerator.getInstance();

    @Override
    public void onCreate() {
        super.onCreate();
        serviceGenerator.generateRest();

    }

}
